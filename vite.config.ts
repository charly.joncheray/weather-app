import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import path from "path";
import svgr from "vite-plugin-svgr";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svgr(), react()],
  resolve: {
    alias: {
      "@": path.join(path.resolve(__dirname, "./src")),
      "@assets": path.resolve(__dirname, "src/assets"),
      "@configuration": path.join(path.resolve(__dirname, "src/configuration")),
      "@hooks": path.join(path.resolve(__dirname, "src/hooks")),
      "@mocks": path.join(path.resolve(__dirname, "src/mocks")),
      "@providers": path.join(path.resolve(__dirname, "src/providers")),
      "@routes": path.join(path.resolve(__dirname, "src/routes")),
      "@services": path.join(path.resolve(__dirname, "src/services")),
      "@weather-api": path.resolve(__dirname, "src/generated-sources/openapi"),
      "@UI": path.resolve(__dirname, "src/UI"),
    },
  },
});
