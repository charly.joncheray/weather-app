import { ReactNode, createContext, useContext, useReducer } from "react";

export enum TemperatureUnit {
  celcius = "c",
  fahrenheit = "f",
}

export enum DistanceUnit {
  miles = "miles",
  kilometer = "km",
}

export enum SpeedUnit {
  milesperhour = "mph",
  kilometerperhour = "kph",
}

export enum PressureUnit {
  millibar = "mb",
  inche = "in",
}

interface State {
  temperatureUnit: TemperatureUnit;
  distanceUnit: DistanceUnit;
  speedUnit: SpeedUnit;
  pressureUnit: PressureUnit;
}

export enum ActionType {
  CHANGE_TEMPERATURE_UNIT = "change_temperature_unit",
  CHANGE_DISTANCE_UNIT = "change_distance_unit",
  CHANGE_SPEED_UNIT = "change_speed_unit",
  CHANGE_PRESSURE_UNIT = "change_pressure_unit",
}

interface Action {
  type: ActionType;
}

export type Dispatch = (action: Action) => void;

const UnitContext = createContext<{ state: State; dispatch: Dispatch } | undefined>(undefined);

function temperatureReducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.CHANGE_DISTANCE_UNIT:
      return {
        ...state,
        distanceUnit: state.distanceUnit === DistanceUnit.kilometer ? DistanceUnit.miles : DistanceUnit.kilometer,
      };
    case ActionType.CHANGE_PRESSURE_UNIT:
      return {
        ...state,
        pressureUnit: state.pressureUnit === PressureUnit.inche ? PressureUnit.millibar : PressureUnit.inche,
      };
    case ActionType.CHANGE_SPEED_UNIT:
      return {
        ...state,
        speedUnit: state.speedUnit === SpeedUnit.kilometerperhour ? SpeedUnit.milesperhour : SpeedUnit.kilometerperhour,
      };
    case ActionType.CHANGE_TEMPERATURE_UNIT:
      return {
        ...state,
        temperatureUnit:
          state.temperatureUnit === TemperatureUnit.celcius ? TemperatureUnit.fahrenheit : TemperatureUnit.celcius,
      };
    default:
      return state;
  }
}

interface UnitProviderProps {
  children: ReactNode;
}

function UnitProvider({ children }: UnitProviderProps) {
  const [state, dispatch] = useReducer(temperatureReducer, {
    temperatureUnit: TemperatureUnit.celcius,
    distanceUnit: DistanceUnit.kilometer,
    speedUnit: SpeedUnit.kilometerperhour,
    pressureUnit: PressureUnit.millibar,
  });
  const value = { state, dispatch };

  return <UnitContext.Provider value={value}>{children}</UnitContext.Provider>;
}

function useUnitContext() {
  const context = useContext(UnitContext);
  if (context === undefined) {
    throw new Error("useUnitContext must be used within a UnitProvider");
  }
  return context;
}

export { UnitProvider, useUnitContext };
