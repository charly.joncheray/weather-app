import { mergeQueryKeys } from "@lukemorales/query-key-factory";

import { forecastKeys } from "./ForecastServices";
import { locationsKeys } from "./LocationServices";

export * from "./WeatherApi";

export const queries = mergeQueryKeys(forecastKeys, locationsKeys);
