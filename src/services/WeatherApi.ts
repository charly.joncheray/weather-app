import { ForecastApi, LocationApi } from "@weather-api/api";
import { Configuration as WeatherApiConfiguration } from "@weather-api/configuration";
import axios, { AxiosInstance } from "axios";

interface Configuration {
  basePath: string;
}

export class WeatherApi {
  private static instance: WeatherApi;
  private baseConfiguration: WeatherApiConfiguration;

  private axiosInstace: AxiosInstance;

  public locationApi: LocationApi;
  public forecastApi: ForecastApi;

  private constructor(configuration?: Configuration) {
    this.baseConfiguration = new WeatherApiConfiguration({
      basePath: configuration?.basePath,
    });
    this.axiosInstace = axios.create();

    this.forecastApi = new ForecastApi(this.baseConfiguration, "", this.axiosInstace);
    this.locationApi = new LocationApi(this.baseConfiguration, "", this.axiosInstace);
  }

  static getInstance() {
    if (!WeatherApi.instance) {
      throw new Error("No WeatherApi instance created");
    }
    return WeatherApi.instance;
  }

  static createInstance(configuration?: Configuration) {
    if (!WeatherApi.instance) {
      WeatherApi.instance = new WeatherApi(configuration);
    }
    return WeatherApi.instance;
  }
}
