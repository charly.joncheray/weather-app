import { config, ENV } from "@configuration";
import { createQueryKeys } from "@lukemorales/query-key-factory";
import { WeatherApi } from "./WeatherApi";

const getForecasts = async (q: string) => {
  const forecasts = await WeatherApi.getInstance().forecastApi.getForecasts(config(ENV.WEATHER_API_KEY), q, 6);
  return forecasts.data;
};

export const forecastKeys = createQueryKeys("forecasts", {
  detail: (q: string) => ({
    queryKey: [q],
    queryFn: () => getForecasts(q),
  }),
});
