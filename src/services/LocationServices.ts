import { config, ENV } from "@configuration";
import { createQueryKeys } from "@lukemorales/query-key-factory";
import { Location } from "@weather-api/models";
import { WeatherApi } from "./WeatherApi";

const getLocations = async (q: string): Promise<Location[]> => {
  const locations = await WeatherApi.getInstance().locationApi.getLocations(config(ENV.WEATHER_API_KEY), q);
  return locations.data;
};

export const locationsKeys = createQueryKeys("locations", {
  list: (q: string) => ({
    queryKey: [q],
    queryFn: () => getLocations(q),
  }),
});
