import React from "react";
import ReactDOM from "react-dom/client";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { RouterProvider } from "react-router-dom";

import { ENV, checkConfig, config } from "@configuration";
import router from "@routes";
import { WeatherApi } from "@services";

import "./main.scss";

checkConfig();

WeatherApi.createInstance({
  basePath: config(ENV.WEATHER_API_BASE_URL),
});

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 1000 * 60 * 5,
    },
  },
});

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router(queryClient)} />
      <ReactQueryDevtools />
    </QueryClientProvider>
  </React.StrictMode>
);
