import { RouteObject } from "react-router-dom";

import { Location, loader as locationLoader } from "@UI/pages/Location/Location";
import { QueryClient } from "@tanstack/react-query";

export const LocationRoutes: (queryClient: QueryClient) => RouteObject[] = (queryClient) => [
  {
    path: "/locations",
    element: <Location />,
    loader: locationLoader(queryClient),
  },
];
