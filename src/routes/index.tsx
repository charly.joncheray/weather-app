import { Navigate, createBrowserRouter } from "react-router-dom";
import { LocationRoutes } from "./location";
import { QueryClient } from "@tanstack/react-query";
import { Root } from "@UI/pages";

const router = (queryClient: QueryClient) =>
  createBrowserRouter([
    {
      path: "/",
      element: <Root />,
    },
    ...LocationRoutes(queryClient),
  ]);

export default router;
