import clsx from "clsx";

import { ActionButton, ActionButtonProps } from "@UI/components";

import styles from "./ActionButtonGroup.module.scss";

interface ActionButtonGroupProps {
  buttonOne: ActionButtonProps;
  buttonTwo: ActionButtonProps;
  active: 1 | 2;
  dispatch: () => void;
}

export const ActionButtonGroup: React.FC<ActionButtonGroupProps> = ({ buttonOne, buttonTwo, active, dispatch }) => (
  <div className={styles.actionButtonGroup}>
    <ActionButton
      className={clsx(styles.actionButtonGroup__button, active === 1 && styles["actionButtonGroup__button--active"])}
      {...buttonOne}
      onClick={dispatch}
      disabled={active === 1}
    />
    <ActionButton
      className={clsx(styles.actionButtonGroup__button, active === 2 && styles["actionButtonGroup__button--active"])}
      {...buttonTwo}
      onClick={dispatch}
      disabled={active === 2}
    />
  </div>
);
