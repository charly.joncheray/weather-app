import { Location } from "@weather-api/models";

import { Location as LocationComponent } from "@UI/components";

import styles from "./LocationList.module.scss";

interface LocationListProps {
  data?: Location[];
  isLoading: boolean;
}

export const LocationList: React.FC<LocationListProps> = ({ data, isLoading }: LocationListProps) => {
  if (isLoading) {
    return <></>;
  }

  if (!data?.length) {
    return <></>;
  }

  return (
    <ul className={styles.locationList}>
      {data.map((location, key) => (
        <LocationComponent key={key} location={location.name} country={location.country} />
      ))}
    </ul>
  );
};
