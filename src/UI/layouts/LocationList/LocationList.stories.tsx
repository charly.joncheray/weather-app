import type { Meta, StoryObj } from "@storybook/react";

import { locationDto } from "@mocks";

import { LocationList } from "./LocationList";
import { MemoryRouter } from "react-router-dom";

const meta = {
  title: "Layouts/LocationList",
  component: LocationList,
  decorators: [
    (Story) => (
      <MemoryRouter>
        <Story />
      </MemoryRouter>
    ),
  ],
} satisfies Meta<typeof LocationList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    isLoading: false,
    data: locationDto,
  },
};
