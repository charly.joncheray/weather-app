import type { Meta, StoryObj } from "@storybook/react";

import { Main } from "./Main";
import { UnitProvider } from "@providers";
import { forecastDto } from "@mocks";

const meta = {
  title: "Layouts/Main",
  component: Main,
  parameters: {
    layout: "fullscreen",
  },
  decorators: [
    (Story) => (
      <UnitProvider>
        <Story />
      </UnitProvider>
    ),
  ],
} satisfies Meta<typeof Main>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    ...forecastDto,
  },
};
