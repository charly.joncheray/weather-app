import { Forecast } from "@weather-api/models";

import { PercentageBar, WindCompass } from "@UI/components";
import { DayCardList, Header, HightlightCard } from "@UI/layouts";

import styles from "./Main.module.scss";
import { useUnitContext } from "@providers";

export const Main: React.FC<Forecast> = (props) => {
  const { state } = useUnitContext();

  return (
    <main className={styles.main}>
      <Header className={styles.main_header} />
      <DayCardList forecastday={props.forecast.forecastday} />
      <h1 className={styles.main__title}>Today’s Hightlights</h1>
      <div className={styles.main__hightlights}>
        <HightlightCard
          title={"Wind status"}
          value={props.current[`wind_${state.speedUnit}`].toString()}
          unity={state.speedUnit}
          indicator={<WindCompass windDirection={props.current.wind_dir} />}
        />
        <HightlightCard
          title="Humidity"
          value={props.current.humidity.toString()}
          unity={"%"}
          indicator={<PercentageBar percent={props.current.humidity} />}
        />
        <HightlightCard
          title={"Visibility"}
          value={props.current[`vis_${state.distanceUnit}`].toString()}
          unity={state.distanceUnit}
        />
        <HightlightCard
          title={"Air Pressure"}
          value={props.current[`pressure_${state.pressureUnit}`].toString()}
          unity={state.pressureUnit}
        />
      </div>
    </main>
  );
};
