import React, { useState } from "react";
import moment from "moment";

import { Current, Location } from "@weather-api/models";

import { Button, ActionButton } from "@UI/components";
import { Menu } from "@UI/layouts";
import { useUnitContext } from "@providers";

import { ReactComponent as Cloud1 } from "@assets/svg/cloud_1.svg";
import { ReactComponent as Cloud2 } from "@assets/svg/cloud_2.svg";
import { ReactComponent as Cloud3 } from "@assets/svg/cloud_3.svg";
import { ReactComponent as Cloud4 } from "@assets/svg/cloud_4.svg";
import styles from "./Aside.module.scss";

interface AsideProps {
  current: Current;
  location: Location;
}

export const Aside: React.FC<AsideProps> = ({ current, location }) => {
  const { state } = useUnitContext();

  const [displayMenu, setDisplayMenu] = useState(false);

  return (
    <React.Fragment>
      <aside className={styles.aside}>
        <div className={styles.aside__buttons}>
          <Button
            className={styles.aside__buttons__button}
            title={"Search for places"}
            onClick={() => setDisplayMenu(true)}
          />
          <ActionButton logo={"my_location"} />
        </div>
        <div className={styles.aside__images}>
          <img src={current.condition.icon} alt="" />
          <div className={styles.aside__images__clouds}>
            <Cloud1 />
            <Cloud2 />
            <Cloud3 />
            <Cloud4 />
          </div>
        </div>
        <div className={styles.aside__informations}>
          <h1 className={styles.aside__informations__temperature}>
            {current[`temp_${state.temperatureUnit}`]}
            <span>°{state.temperatureUnit.toUpperCase()}</span>
          </h1>
          <h2 className={styles.aside__informations__condition}>{current.condition.text}</h2>
          <div className={styles.aside__informations__footer}>
            <div className={styles.aside__informations__footer__date}>
              <span>Today</span>
              <span>·</span>
              <span>{moment(location.localtime).format("dd, D MMM")}</span>
            </div>
            <p className={styles.aside__informations__footer__location}>
              <span className="material-icons">place</span>
              {location.name}
            </p>
          </div>
        </div>
      </aside>
      <Menu display={displayMenu} setDisplay={setDisplayMenu} />
    </React.Fragment>
  );
};
