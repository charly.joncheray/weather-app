import type { Meta, StoryObj } from "@storybook/react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

import { forecastDto } from "@mocks";
import { UnitProvider } from "@providers";

import { Aside } from "./Aside";
import { MemoryRouter } from "react-router-dom";

const client = new QueryClient({
  defaultOptions: {
    queries: {
      cacheTime: 0,
    },
  },
});

const meta = {
  title: "Layouts/Aside",
  component: Aside,
  parameters: {
    layout: "fullscreen",
  },
  decorators: [
    (Story) => (
      <QueryClientProvider client={client}>
        <MemoryRouter>
          <UnitProvider>
            <Story />
          </UnitProvider>
        </MemoryRouter>
      </QueryClientProvider>
    ),
  ],
} satisfies Meta<typeof Aside>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    current: forecastDto.current,
    location: forecastDto.location,
  },
};
