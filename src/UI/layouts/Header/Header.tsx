import clsx from "clsx";

import { ActionType, DistanceUnit, PressureUnit, SpeedUnit, TemperatureUnit, useUnitContext } from "@providers";
import { ActionButtonGroup } from "@UI/layouts";

import styles from "./Header.module.scss";

interface HeaderProps {
  className?: string;
}

export const Header: React.FC<HeaderProps> = ({ className }) => {
  const { state, dispatch } = useUnitContext();
  return (
    <div className={clsx(styles.header, className)}>
      <ActionButtonGroup
        buttonOne={{ logo: "mi" }}
        buttonTwo={{ logo: "km" }}
        dispatch={() => dispatch({ type: ActionType.CHANGE_DISTANCE_UNIT })}
        active={state.distanceUnit === DistanceUnit.miles ? 1 : 2}
      />
      <ActionButtonGroup
        buttonOne={{ logo: "mph" }}
        buttonTwo={{ logo: "kph" }}
        dispatch={() => dispatch({ type: ActionType.CHANGE_SPEED_UNIT })}
        active={state.speedUnit === SpeedUnit.milesperhour ? 1 : 2}
      />
      <ActionButtonGroup
        buttonOne={{ logo: "mb" }}
        buttonTwo={{ logo: "in" }}
        dispatch={() => dispatch({ type: ActionType.CHANGE_PRESSURE_UNIT })}
        active={state.pressureUnit === PressureUnit.millibar ? 1 : 2}
      />
      <ActionButtonGroup
        buttonOne={{ logo: "°C" }}
        buttonTwo={{ logo: "°F" }}
        dispatch={() => dispatch({ type: ActionType.CHANGE_TEMPERATURE_UNIT })}
        active={state.temperatureUnit === TemperatureUnit.celcius ? 1 : 2}
      />
    </div>
  );
};
