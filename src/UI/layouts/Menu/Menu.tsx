import { Dispatch, useEffect, useState } from "react";
import clsx from "clsx";
import { useQuery } from "@tanstack/react-query";

import { useDebounce } from "@hooks";
import { queries } from "@services";
import { LocationList, SearchForm } from "@UI/layouts";

import styles from "./Menu.module.scss";
import { useLocation } from "react-router-dom";

interface MenuProps {
  display: boolean;
  setDisplay: Dispatch<React.SetStateAction<boolean>>;
}

export const Menu = ({ display = false, setDisplay }: MenuProps) => {
  const route = useLocation();
  const [location, setLocation] = useState<string>("");
  const debouncedLocation = useDebounce(location, 200);

  const { data, isLoading } = useQuery({
    ...queries.locations.list(debouncedLocation),
    enabled: Boolean(debouncedLocation),
  });

  useEffect(() => {
    !display && setLocation("");
  }, [display]);

  useEffect(() => {
    setDisplay(false);
  }, [route]);

  return (
    <aside className={clsx(styles.menu, display && styles["menu--displayed"])}>
      <div className={styles.menu__header}>
        <span className="material-icons-outlined" onClick={() => setDisplay(false)}>
          close
        </span>
      </div>
      <SearchForm location={location} setLocation={setLocation} displayed={display} />
      {data && <LocationList data={data} isLoading={isLoading} />}
    </aside>
  );
};
