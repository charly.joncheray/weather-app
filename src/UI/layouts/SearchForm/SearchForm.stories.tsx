import type { Decorator, Meta, StoryObj } from "@storybook/react";
import { createMemoryRouter, RouterProvider } from "react-router-dom";

import { SearchForm } from "./SearchForm";

const reactRouterDecorator: Decorator = (Story) => {
  const routes = [
    {
      path: "/",
      element: <Story />,
    },
  ];
  const router = createMemoryRouter(routes, {
    initialEntries: ["/"],
  });

  return <RouterProvider router={router} />;
};

const meta = {
  title: "Layouts/SearchForm",
  component: SearchForm,
  decorators: [reactRouterDecorator],
} satisfies Meta<typeof SearchForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
