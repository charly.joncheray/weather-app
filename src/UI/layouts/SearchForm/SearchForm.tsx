import { Dispatch, useCallback, useEffect, useRef } from "react";

import { Input, Button } from "@UI/components";

import styles from "./SearchForm.module.scss";

interface SearchFormProps {
  location: string;
  setLocation: Dispatch<React.SetStateAction<string>>;
  displayed: boolean;
}

export const SearchForm: React.FC<SearchFormProps> = ({ location, setLocation, displayed }: SearchFormProps) => {
  const input = useRef<HTMLInputElement>(null);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLocation(e.target.value);
  };

  useEffect(() => {
    if (input.current) {
      input.current.focus();
    }
  }, [displayed]);

  return (
    <div className={styles.searchForm}>
      <div className={styles.searchForm__input}>
        <span className="material-icons-round">search</span>
        <Input
          id="q"
          aria-label="Search location"
          placeholder="search location"
          type="search"
          name="q"
          value={location}
          onChange={handleChange}
          ref={input}
        />
      </div>
      {/* <Button title="Search" type="submit" /> */}
    </div>
  );
};
