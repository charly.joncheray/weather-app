import { ForecastForecast } from "@weather-api/models";

import styles from "./DayCardList.module.scss";
import { DayCard } from "@UI/components";

export const DayCardList: React.FC<ForecastForecast> = ({ forecastday }) => (
  <div className={styles.dayCardList}>{forecastday.map((day, key) => key !== 0 && <DayCard key={key} {...day} />)}</div>
);
