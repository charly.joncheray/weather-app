import type { Meta, StoryObj } from "@storybook/react";

import { forecastDto } from "@mocks";
import { UnitProvider } from "@providers";

import { DayCardList } from "./DayCardList";

const meta = {
  title: "Layouts/DayCardList",
  component: DayCardList,
  decorators: [
    (Story) => (
      <UnitProvider>
        <Story />
      </UnitProvider>
    ),
  ],
} satisfies Meta<typeof DayCardList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    forecastday: forecastDto.forecast.forecastday,
  },
};
