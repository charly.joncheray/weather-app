import styles from "./HighlightCard.module.scss";

interface HightlightCardProps {
  title: string;
  value: string;
  unity: string;
  indicator?: React.ReactNode;
}

export const HightlightCard: React.FC<HightlightCardProps> = ({ title, value, unity, indicator }): JSX.Element => (
  <div className={styles.hightlightCard}>
    <h1 className={styles.hightlightCard__title}>{title}</h1>
    <p className={styles.hightlightCard__data}>
      {value}
      <span className={styles.hightlightCard__unity}>{unity}</span>
    </p>
    <div className={styles.hightlightCard__indicator}>{indicator}</div>
  </div>
);
