import type { Meta, StoryObj } from "@storybook/react";

import { HightlightCard } from "./HighlightCard";
import { PercentageBar, WindCompass } from "@UI/components";

const meta = {
  title: "Layouts/HightlightCard",
  component: HightlightCard,
} satisfies Meta<typeof HightlightCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    title: "Visibility",
    value: "6,4",
    unity: "miles",
  },
};

export const WithPercentageIndicator: Story = {
  args: {
    title: "Humidity",
    value: "84",
    unity: "%",
    indicator: <PercentageBar percent={84} />,
  },
};

export const WithCompassIndicator: Story = {
  args: {
    title: "Wind status",
    value: "7",
    unity: "mph",
    indicator: <WindCompass windDirection={"WSW"} />,
  },
};
