import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export * from "./Location/Location";

interface locationState {
  latitude?: string;
  longitude?: string;
}

export const Root: React.FC = () => {
  const navigate = useNavigate();
  const [location, setLocation] = useState<locationState>();
  const [error, setError] = useState<string>("");

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position: GeolocationPosition) => {
        setLocation({
          latitude: position.coords.latitude.toString(),
          longitude: position.coords.longitude.toString(),
        });
      },
      (error: GeolocationPositionError) => {
        console.log(error.message);
        setError(error.message);
      }
    );
  });

  useEffect(() => {
    !!error && navigate("/locations?q=Angers");
  }, [error]);

  useEffect(() => {
    location && navigate(`/locations?q=${location?.latitude},${location?.longitude}`);
  }, [location]);

  return <p>Loading ...</p>;
};
