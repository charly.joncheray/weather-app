import React from "react";
import { QueryClient, useQuery } from "@tanstack/react-query";
import { useLoaderData } from "react-router-dom";

import { UnitProvider } from "@providers";
import { queries } from "@services";
import { Aside, Main } from "@UI/layouts";

import styles from "./Location.module.scss";

export const loader =
  (queryClient: QueryClient) =>
  async ({ request }) => {
    const url = new URL(request.url);
    const q = url.searchParams.get("q") ?? "Angers";
    if (!queryClient.getQueryData(queries.forecasts.detail(q).queryKey)) {
      await queryClient.fetchQuery(queries.forecasts.detail(q));
    }
    return { q };
  };

export const Location: React.FC = () => {
  const { q } = useLoaderData() as Awaited<ReturnType<ReturnType<typeof loader>>>;
  const { data } = useQuery(queries.forecasts.detail(q));

  if (!data) {
    // TODO : gestion erreur
    return <></>;
  }

  return (
    <UnitProvider>
      <div className={styles.location}>
        <Aside current={data.current} location={data.location} />
        <Main {...data} />
      </div>
    </UnitProvider>
  );
};
