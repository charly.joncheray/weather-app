import clsx from "clsx";

import styles from "./Button.module.scss";

interface ButtonProps
  extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  title: string;
}

export const Button: React.FC<ButtonProps> = ({ title, className, ...props }): JSX.Element => (
  <button className={clsx(styles.button, className)} {...props}>
    {title}
  </button>
);
