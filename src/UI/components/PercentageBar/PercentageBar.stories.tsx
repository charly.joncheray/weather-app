import type { Meta, StoryObj } from "@storybook/react";

import { PercentageBar } from "./PercentageBar";

const meta = {
  title: "Components/PercentageBar",
  component: PercentageBar,
} satisfies Meta<typeof PercentageBar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    percent: 84,
  },
};

export const Zero: Story = {
  args: {
    percent: 0,
  },
};

export const Hundred: Story = {
  args: {
    percent: 100,
  },
};
