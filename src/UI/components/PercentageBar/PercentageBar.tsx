import { useEffect, useState } from "react";
import styles from "./PercentageBar.module.scss";

interface PercentageBarProps {
  percent: number;
}

export const PercentageBar: React.FC<PercentageBarProps> = ({ percent }) => {
  const [value, setValue] = useState(0);

  useEffect(() => {
    setValue(percent);
  });

  return (
    <div className={styles.percentageBar}>
      <div className={styles.percentageBar__indicators}>
        <p>0</p>
        <p>50</p>
        <p>100</p>
      </div>
      <div className={styles.percentageBar__bar}>
        <div className={styles.percentageBar__value} style={{ width: `${value}%` }} />
      </div>
      <div className={styles.percentageBar__unity}>%</div>
    </div>
  );
};
