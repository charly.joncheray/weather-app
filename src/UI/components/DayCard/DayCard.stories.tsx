import type { Meta, StoryObj } from "@storybook/react";

import { forecastDto } from "@mocks";

import { DayCard } from "./DayCard";
import { UnitProvider } from "@providers";

const meta = {
  title: "Components/DayCard",
  component: DayCard,
  decorators: [
    (Story) => (
      <UnitProvider>
        <Story />
      </UnitProvider>
    ),
  ],
} satisfies Meta<typeof DayCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    ...forecastDto.forecast.forecastday[0],
  },
};
