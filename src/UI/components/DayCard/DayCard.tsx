import moment from "moment";

import { Forecastday } from "@weather-api/models";

import { useUnitContext } from "@providers";

import styles from "./DayCard.module.scss";

export const DayCard: React.FC<Forecastday> = (props) => {
  const { state } = useUnitContext();

  const isTomorrow = moment().add(1, "day").isSame(moment(props.date), "day");

  return (
    <div className={styles.dayCard}>
      <h1 className={styles.dayCard__day}>{isTomorrow ? "Tomorrow" : moment(props.date).format("dd, D MMM")}</h1>
      <img className={styles.dayCard__image} src={props.day.condition.icon} alt="" />
      <div className={styles.dayCard__temperatures}>
        <p>
          {props.day[`mintemp_${state.temperatureUnit}`]}°{state.temperatureUnit.toUpperCase()}
        </p>
        <p>
          {props.day[`maxtemp_${state.temperatureUnit}`]}°{state.temperatureUnit.toUpperCase()}
        </p>
      </div>
    </div>
  );
};
