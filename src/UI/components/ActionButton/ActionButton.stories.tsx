import type { Meta, StoryObj } from "@storybook/react";

import { ActionButton } from "./ActionButton";

const meta = {
  title: "Components/ActionButton",
  component: ActionButton,
} satisfies Meta<typeof ActionButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Location: Story = {
  args: {
    logo: "my_location",
  },
};

export const Celsius: Story = {
  args: {
    logo: "°C",
  },
};

export const Fahrenheit: Story = {
  args: {
    logo: "°F",
  },
};
