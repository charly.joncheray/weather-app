import clsx from "clsx";

import styles from "./ActionButton.module.scss";

export interface ActionButtonProps
  extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  logo: "my_location" | "°C" | "°F" | "mi" | "km" | "mph" | "kph" | "mb" | "in";
  className?: string;
}

export const ActionButton: React.FC<ActionButtonProps> = ({ logo, className, ...props }) => (
  <button className={clsx(styles.actionButton, className)} {...props}>
    {logo === "my_location" ? <span className="material-icons-round">{logo}</span> : <span>{logo}</span>}
  </button>
);
