import clsx from "clsx";

import styles from "./Input.module.scss";
import { forwardRef } from "react";

interface InputProps extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {}

export const Input = forwardRef<HTMLInputElement, InputProps>(({ className, ...props }, ref) => (
  <input
    id={props.id}
    type={props.type}
    placeholder={props.placeholder}
    name={props.name}
    className={clsx(styles.searchInput, className)}
    value={props.value}
    onChange={props.onChange}
    ref={ref}
  />
));
