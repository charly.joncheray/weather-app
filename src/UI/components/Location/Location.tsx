import { Link } from "react-router-dom";
import styles from "./Location.module.scss";

interface LocationProps {
  location: string;
  country: string;
}

export const Location: React.FC<LocationProps> = ({ location, country }) => (
  <li className={styles.location}>
    <Link to={`/locations?q=${location}`} className={styles.location__link}>
      {location}, {country}
      <span className="material-icons-round">navigate_next</span>
    </Link>
  </li>
);
