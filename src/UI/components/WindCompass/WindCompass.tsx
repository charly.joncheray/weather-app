import { WindDirectionEnum } from "@weather-api/models";
import clsx from "clsx";

import styles from "./WindCompass.module.scss";

interface WindCompassProps {
  windDirection: WindDirectionEnum;
}

export const WindCompass: React.FC<WindCompassProps> = ({ windDirection }: WindCompassProps): JSX.Element => (
  <div className={styles.windCompass}>
    <div className={clsx(styles.windCompass__compass, styles[`windCompass__compass--${windDirection}`])}>
      <span className="material-icons-round">navigation</span>
    </div>
    <p className={styles.windCompass__direction}>{windDirection}</p>
  </div>
);
