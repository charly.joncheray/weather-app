import type { Meta, StoryObj } from "@storybook/react";
import { WindDirectionEnum } from "@weather-api/models";

import { WindCompass } from "./WindCompass";

const meta = {
  title: "Components/WindCompass",
  component: WindCompass,
} satisfies Meta<typeof WindCompass>;

export default meta;
type Story = StoryObj<typeof meta>;

export const North: Story = {
  args: {
    windDirection: WindDirectionEnum.N,
  },
};

export const South: Story = {
  args: {
    windDirection: WindDirectionEnum.S,
  },
};

export const West: Story = {
  args: {
    windDirection: WindDirectionEnum.W,
  },
};

export const East: Story = {
  args: {
    windDirection: WindDirectionEnum.E,
  },
};
