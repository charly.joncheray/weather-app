import { $enum } from "ts-enum-util";

export enum ENV {
  ROOT_URL = "ROOT_URL",
  WEATHER_API_BASE_URL = "WEATHER_API_BASE_URL",
  WEATHER_API_KEY = "WEATHER_API_KEY",
}

export const config = (key: ENV) => {
  switch (key) {
    case ENV.ROOT_URL:
      return `${window.location.protocol}//${window.location.hostname}${
        window.location.port ? `:${window.location.port}` : ""
      }`;
    default:
      return import.meta.env[`VITE_${key}`];
  }
};

export const checkConfig = () => {
  if (process.env.NODE_ENV != "production") {
    process.env.NODE_ENV
      ? console.info("Defined env is " + process.env.NODE_ENV)
      : console.error("Environment variables loaded from environment ❌");
  }

  $enum(ENV).forEach((value: any) => {
    config(value)?.length ?? console.warn("Environment variable VITE_" + value + " is not set !");
  });
};
