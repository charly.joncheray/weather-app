import type { Preview } from "@storybook/react";

import "../src/main.scss";
import { WeatherApi } from "../src/services";

// Initialize WeatherApi
WeatherApi.createInstance();

const preview: Preview = {
  parameters: {
    backgrounds: {
      default: "light",
    },
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
